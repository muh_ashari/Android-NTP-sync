package com.nostratech.clock.sync;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.widget.Toast;

/**
 * Utilities
 * @author andri.khrisharyadi@nostratech.com
 */
public final class Utils {

	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss z";
	
	/**
	 * Convert milliseconds to time format (hh:mm:ss) 
	 * @param time
	 * @return
	 */
	public static String formatTime(long time) {
		String res = "";
		res += time/60 + ":";
		if (time % 60 < 10)
			res += "0";
		res += (time % 60);
		return res;
	}
	
	/**
	 * Date to string format
	 * @param date
	 * @return
	 */
	public static String dateToString(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		return sdf.format(date);
	}
	
	/**
	 * Show {@link Toast} message
	 * @param context
	 * @param message
	 */
	public static void showMessage(Context context, String message){
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}
	
	/**
     * Check if internet connection available
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context){
    	ConnectivityManager conMgr = 
    			(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {

            return true;

        } else {
            return false;

        }
    }
    
    /**
	 * Popup warning no internet connection
	 * @param activity
	 */
	public static void showInfoNoNetwork(final Activity activity) {
		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

		alertDialog.setTitle(R.string.error_title);
		alertDialog.setMessage(activity.getString(R.string.error_no_network));
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, 
				"EXIT", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				activity.finish();
			}
		} );		
		
		alertDialog.show();
	}
	
}
