package com.nostratech.clock.sync;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;
import org.apache.commons.net.ntp.TimeStamp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.nostratech.clock.sync.Utils.formatTime;
import static com.nostratech.clock.sync.Utils.showMessage;
import static com.nostratech.clock.sync.Utils.isNetworkConnected;
import static com.nostratech.clock.sync.Utils.showInfoNoNetwork;
import static com.nostratech.clock.sync.Utils.dateToString;

/**
 * Main Activity
 * <br>Time syncronize using apache common-net : 
 * <a href="http://commons.apache.org/proper/commons-net/"> commons.apache.org/proper/commons-net </a>
 * @author andri.khrisharyadi@nostratech.com
 */
public class MainActivity extends Activity {
	
	private static final String TAG = "MainActivity";
	private CustomAnalogClock analogClock;
	private TextView nextSync;
	private TextView lastSync;
	private TextView txtNtpServer;
	private Button syncNow;
	
	private static final int MINUTES_SYNC_TIME = 10;
	private Date defaultDate = new Date();
	
	private ProgressDialog progressBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		checkInternet();
		initComponent();
		initComponentAction();
	}
	
	@Override
	protected void onDestroy() {
		countDownTimer.cancel();
		super.onDestroy();
	}
	
	/**
	 * Initial components
	 */
	private void initComponent(){
		
		progressBar = new ProgressDialog(this);
		progressBar.setCancelable(false);
		progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		nextSync = (TextView) findViewById(R.id.next_sync);
		lastSync = (TextView) findViewById(R.id.last_sync);
		txtNtpServer = (TextView) findViewById(R.id.txt_ntp_server);
		syncNow = (Button) findViewById(R.id.btnSync);
		
		nextSync.setText(getString(R.string.next_sync) + formatTime( (1000 * 60 * MINUTES_SYNC_TIME) / 1000 ) );
		lastSync.setText(getString(R.string.last_sync_time) + " -");
		txtNtpServer.setText(getString(R.string.note_ntp_server) + getString(R.string.host_ntp_server) );
		analogClock = (CustomAnalogClock) findViewById(R.id.clock);
		
		//analogClock.forceChangeTime(defaultDate.getTime() - (1000*60) );
		syncTimeToServer();
		countDownTimer.start();
	}
	
	/**
	 * Initial components action
	 */
	private void initComponentAction(){
		syncNow.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				syncTimeToServer();
			}
		});
	}
	
	// countdown timer
	private CountDownTimer countDownTimer = new CountDownTimer( 1000 * 60 * MINUTES_SYNC_TIME
			, 1000) {
		
		@Override
		public void onTick(long millisUntilFinished) {
			nextSync.setText(getString(R.string.next_sync) + formatTime( millisUntilFinished / 1000 ) );
		}
		
		@Override
		public void onFinish() {
			syncTimeToServer();
		}
	};
	
	/**
	 * Check internet connection
	 */
	private boolean checkInternet(){
		if(!isNetworkConnected(this)){
			showInfoNoNetwork(this);
			return false;
		}
		return true;
	}
	
	/**
	 * Handler if time sync is success
	 * @param time
	 */
	private void onSuccessSyncTime(long time){
		nextSync.setText(getString(R.string.next_sync) + formatTime( (1000 * 60 * MINUTES_SYNC_TIME) / 1000 ) );
		lastSync.setText(getString(R.string.last_sync_time) + dateToString(new Date(time)));
		analogClock.forceChangeTime(time );
		countDownTimer.start();
	}
	
	/**
	 * Handler if time sync is failed
	 * @param time
	 */
	private void onFailedSyncTime(long time){
		showMessage(MainActivity.this, "Sorry, sync failed");
		nextSync.setText(getString(R.string.next_sync) + formatTime( (1000 * 60 * MINUTES_SYNC_TIME) / 1000 ) );
		countDownTimer.start();
	}
	
	/**
	 * Sync to NTP server
	 */
	private void syncTimeToServer(){
		progressBar.setMessage("Syncronizing to NTP server");
		progressBar.show();
		new AsyncTask<String, Void, Long>(){

			@Override
			protected Long doInBackground(String... params) {
				final String TIME_SERVER = params[0];
				
				NTPUDPClient timeClient = new NTPUDPClient();
				timeClient.setDefaultTimeout(10000);
				InetAddress inetAddress;
				TimeInfo timeInfo;
				try {
					Log.d(TAG, "NTP request sent, waiting for response...\n");
					inetAddress = InetAddress.getByName(TIME_SERVER);
					timeInfo = timeClient.getTime(inetAddress);
					long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
					Log.d(TAG, "NTP response, return Time "+returnTime);
					return returnTime;
					
				} catch (UnknownHostException e) {
					e.printStackTrace();
					return null;
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				}
				
			}

			@Override
			protected void onPostExecute(Long result) {
				progressBar.cancel();
				if(result != null){
					onSuccessSyncTime(result);
				} else {
					onFailedSyncTime(0);
				}
			}
			
		}.execute(getString(R.string.host_ntp_server));
		
	}
	
}
